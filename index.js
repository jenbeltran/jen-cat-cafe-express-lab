import express from "express";
const app = express();

// allows us to parse json
app.use(express.json());

app.listen(3000, () => console.log("Express server is running"));
